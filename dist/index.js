"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const colors = require("colors");
const auth_1 = require("./network/cryptography/auth");
const core_1 = require("./core");
const serverSocket_1 = require("./network/sockets/serverSocket");
require('dotenv').config();
let startCore = new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
    console.log(colors.bgRed('\nStarting Account Server\n'));
    core_1.default.setupConsole();
    auth_1.default.prepareAuthCryptography();
    yield core_1.default.prepareDatabase();
    // All ok, start server
    resolve(true);
})).then(() => {
    //Core.connectToGameServer()
    let server = new serverSocket_1.default();
    console.log(`Account Server started at port ${process.env.AS_PORT}.`);
    server.start(Number.parseInt(process.env.AS_PORT));
}).catch((err) => {
    console.log(err);
});
//# sourceMappingURL=index.js.map