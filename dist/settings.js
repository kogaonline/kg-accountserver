"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// Database Settings
exports.DB_HOST = '10.211.55.3';
exports.DB_USER = 'root';
exports.DB_PASS = 'test';
exports.DB_BASE = 'kg';
// AccountServer Settings
exports.AS_PORT = 9958;
// GameServer Settings
exports.GS_HOST = 'localhost';
exports.GS_PORT = 9865;
//# sourceMappingURL=settings.js.map