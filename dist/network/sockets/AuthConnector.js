"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class AuthConnector {
    constructor(socket) {
        this._socket = socket;
    }
    sendBuffer(buffer) {
        let _buffer = this.cryptographer.encrypt(buffer);
        this._socket.write(_buffer);
    }
    send(data) {
        this.sendBuffer(data.ToArray());
    }
}
exports.default = AuthConnector;
//# sourceMappingURL=AuthConnector.js.map