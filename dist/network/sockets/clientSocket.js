"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const net = require("net");
const core_1 = require("../../core");
class ClientSocket {
    constructor() {
        this.connected = false;
        this.client = new net.Socket();
    }
    connect(ip, port, resolve) {
        let socket = this;
        socket.client.on("end", (s) => {
            socket.connected = false;
            console.log("Disconnected from Game Server...");
            core_1.default.connectToGameServer();
        });
        return this.client.connect(port, ip, (s) => {
            socket.connected = true;
            console.log('Connected to Game Server.');
            resolve(true);
        });
    }
    send(data) {
        this.client.write(data);
    }
    sendFowardInfo(id, username) {
        let data = Buffer.alloc(8 + username.length);
        data.writeUInt32LE(id, 0);
        data.writeUInt32LE(username.length, 4);
        data.write(username, 8);
        this.send(data);
    }
}
exports.default = ClientSocket;
//# sourceMappingURL=clientSocket.js.map