"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const net = require("net");
const AuthConnector_1 = require("../../network/sockets/AuthConnector");
const auth_1 = require("../../network/cryptography/auth");
const _1059__PasswordSeed_1 = require("../../network/packets/auth/(1059) PasswordSeed");
const _1055__Foward_1 = require("../../network/packets/auth/(1055) Foward");
const _1542__Authentication_1 = require("../../network/packets/auth/(1542) Authentication");
const kernel_1 = require("../../logical/kernel");
const info_1 = require("../../database/account/info");
class ServerSocket {
    constructor() {
        let serverSocket = this;
        this.server = new net.Server(function (socket) {
            try {
                socket.on('data', (data) => serverSocket.receiveData(data, connector));
                let pcs = new _1059__PasswordSeed_1.default();
                pcs.Seed = kernel_1.getRandomInt();
                let connector = new AuthConnector_1.default(socket);
                connector.cryptographer = new auth_1.default();
                connector.passwordSeed = pcs.Seed;
                connector.send(pcs);
                socket.on('error', (data) => {
                    // console.log(`Socket error: ${data}`)
                });
            }
            catch (err) {
                console.log(err);
            }
        });
    }
    start(port) {
        this.server.listen(port);
    }
    receiveData(data, connector) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let buffer = connector.cryptographer.decrypt(data);
                let length = buffer.readUInt16LE(0);
                let id = buffer.readUInt16LE(2);
                if (length != 312) {
                    return;
                }
                let foward = new _1055__Foward_1.default();
                foward.Type = _1055__Foward_1.FowardType.Invalid;
                connector.authInfo = new _1542__Authentication_1.default();
                connector.authInfo.Deserialize(buffer);
                const { username, password, server, resource, macAddr } = connector.authInfo;
                let accountInfo = new info_1.default();
                yield accountInfo.load(username);
                if (accountInfo.exists) {
                    if (accountInfo.password === password) {
                        console.log(`Sending ${server} server info to ${username} -> ${process.env.GS_HOST}:${process.env.GS_PORT}.`);
                        foward.Identifier = accountInfo.id;
                        foward.Type = _1055__Foward_1.FowardType.Ready;
                        foward.IP = process.env.GS_HOST;
                        foward.Port = Number.parseInt(process.env.GS_PORT);
                    }
                    else
                        console.log(`User ${username} sent wrong password.`);
                }
                else
                    console.log(`User ${username} does not exist.`);
                connector.send(foward);
                console.log(`User ${username} access complete.`);
            }
            catch (err) {
                console.log(err.message);
                connector._socket.destroy();
            }
        });
    }
}
exports.default = ServerSocket;
//# sourceMappingURL=serverSocket.js.map