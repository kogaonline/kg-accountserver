"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class BufferConverter {
    static ToUInt32(buffer, offset) {
        let _buffer = Buffer.from(buffer);
        var result = _buffer.readUInt32BE(offset);
        return result;
    }
    static ToInt32(buffer, offset) {
        var result = buffer.readInt32BE(offset);
        return result;
    }
    static ToUInt16(buffer, offset) {
        let _buffer = Buffer.from(buffer);
        var result = _buffer.readUInt16BE(offset);
        return result;
    }
    static ToInt16(buffer, offset) {
        let _buffer = Buffer.from(buffer);
        var result = _buffer.readInt16BE(offset);
        return result;
    }
    static ToUInt8(buffer, offset) {
        let _buffer = Buffer.from(buffer);
        var result = _buffer.readUIntBE(offset, 1);
        return result;
    }
    static ToInt8(buffer, offset) {
        let _buffer = Buffer.from(buffer);
        var result = _buffer.readIntBE(offset, 1);
        return result;
    }
    static ToByte(buffer, offset) {
        let _buffer = Buffer.from(buffer);
        var result = _buffer.readIntBE(offset, 1);
        return result;
    }
}
exports.default = BufferConverter;
//# sourceMappingURL=BufferConverter.js.map