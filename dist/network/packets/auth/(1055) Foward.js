"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const BufferWriter_1 = require("../BufferWriter");
exports.FowardType = {
    Ready: 2,
    Invalid: 1,
    Banned: 12,
    NotActivated: 30,
    ActivateFailed: 31,
    TimedOut: 42,
    AttemptsUsed: 51,
};
class LoginFoward {
    constructor() {
        this._Buffer = Buffer.alloc(52);
        BufferWriter_1.default.WriteUInt16(52, 0, this._Buffer);
        BufferWriter_1.default.WriteUInt16(1055, 2, this._Buffer);
    }
    get Identifier() {
        return this._Buffer.readUInt32LE(4);
    }
    set Identifier(value) {
        BufferWriter_1.default.WriteUInt32(value, 4, this._Buffer);
    }
    get Type() {
        return this._Buffer.readUInt32LE(8);
    }
    set Type(value) {
        BufferWriter_1.default.WriteUInt32(value, 8, this._Buffer);
    }
    get IP() {
        return this._Buffer.toString('utf8', 20, 20 + 16);
    }
    set IP(value) {
        this._Buffer.write(value, 20);
    }
    get Port() {
        return this._Buffer.readUInt16LE(12);
    }
    set Port(value) {
        BufferWriter_1.default.WriteUInt16(value, 12, this._Buffer);
    }
    Deserialize(buffer) {
        //no implementation
    }
    ToArray() {
        return this._Buffer;
    }
    Send(client) {
        client.Send(this._Buffer);
    }
}
exports.default = LoginFoward;
//# sourceMappingURL=(1055) Foward.js.map