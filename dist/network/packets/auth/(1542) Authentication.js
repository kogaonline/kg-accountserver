"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const BufferWriter_1 = require("../BufferWriter");
class Authentication {
    constructor() {
        this._buffer = Buffer.alloc(312);
        BufferWriter_1.default.WriteUInt16(312, 0, this._buffer);
        BufferWriter_1.default.WriteUInt16(1542, 2, this._buffer);
    }
    Deserialize(buffer) {
        if (buffer.length === 312) {
            let data = buffer.toString('utf8', 0, 200);
            let user = data.substr(8, 16).replace(/\0/g, '');
            let pass = data.substr(120, 16).replace(/\0/g, '');
            let serv = data.substr(136, 16).replace(/\0/g, '');
            let mac = data.substr(152, 12).replace(/\0/g, '');
            let res = data.substr(193, 8).replace(/\0/g, '');
            this.username = user;
            this.password = pass;
            this.server = serv;
            this.macAddr = mac;
            this.resource = Number.parseInt(res, 10);
        }
    }
    ToArray() {
        return this._buffer;
    }
    Send(client) {
        client.Send(this._buffer);
    }
}
exports.default = Authentication;
//# sourceMappingURL=(1542) Authentication.js.map