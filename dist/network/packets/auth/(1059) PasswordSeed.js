"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const BufferWriter_1 = require("../BufferWriter");
class PasswordCryptographySeed {
    constructor() {
        this._Buffer = Buffer.alloc(8);
        BufferWriter_1.default.WriteUInt16(8, 0, this._Buffer);
        BufferWriter_1.default.WriteUInt16(1059, 2, this._Buffer);
    }
    get Seed() {
        return this._Buffer.readInt32LE(4);
    }
    set Seed(value) {
        BufferWriter_1.default.WriteInt32(value, 4, this._Buffer);
    }
    Deserialize(buffer) {
        //no implementation
    }
    ToArray() {
        return this._Buffer;
    }
    Send(client) {
        client.Send(this._Buffer);
    }
}
exports.default = PasswordCryptographySeed;
//# sourceMappingURL=(1059) PasswordSeed.js.map