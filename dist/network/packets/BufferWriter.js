"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class BufferWriter {
    static WriteUInt16(arg, offset, buffer) {
        if (!buffer || offset > buffer.length - 1)
            return;
        for (var i = 0; i < 2; i++) {
            buffer[offset + i] = arg >> 0 + i * 8;
        }
    }
    static WriteUInt32(arg, offset, buffer) {
        if (!buffer || offset > buffer.length - 1)
            return;
        if (buffer.length >= offset + 4) {
            buffer[offset] = arg;
            buffer[offset + 1] = (arg >> 8);
            buffer[offset + 2] = (arg >> 16);
            buffer[offset + 3] = (arg >> 24);
        }
    }
    static WriteInt32(arg, offset, buffer) {
        for (var i = 0; i < 4; i++) {
            buffer[offset + i] = arg >> 0 + i * 8;
        }
    }
}
exports.default = BufferWriter;
//# sourceMappingURL=BufferWriter.js.map