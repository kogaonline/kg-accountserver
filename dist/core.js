"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const database_1 = require("./database");
const console_1 = require("./console");
const clientSocket_1 = require("./network/sockets/clientSocket");
class Core {
    static prepareDatabase() {
        return __awaiter(this, void 0, void 0, function* () {
            console.log('Preparing Database...');
            return yield Core.database.create();
        });
    }
    static setupConsole() {
        console_1.default.setup();
    }
    static connectToGameServer() {
        return __awaiter(this, void 0, void 0, function* () {
            Core.gameServerSocket = new clientSocket_1.default();
            return yield new Promise(function (resolve, reject) {
                return __awaiter(this, void 0, void 0, function* () {
                    let connected = false;
                    while (!connected) {
                        connected = yield new Promise(function (resolve, reject) {
                            return __awaiter(this, void 0, void 0, function* () {
                                let socket = Core.gameServerSocket;
                                yield socket.connect('localhost', 9865, resolve).on('error', (err) => {
                                    console.log(err);
                                    console.log('Retrying connection to Game Server...');
                                    setTimeout(() => { resolve(false); }, 5000);
                                });
                            });
                        });
                    }
                    resolve(true);
                });
            });
        });
    }
}
Core.database = new database_1.default();
Core.gameServerSocket = null;
exports.default = Core;
//# sourceMappingURL=core.js.map