"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("../../core");
function validateAccess(user, mac, res = 0) {
    return __awaiter(this, void 0, void 0, function* () {
        let accountInfos = yield core_1.default.database.executeRaw(`SELECT * FROM \`accounts\` WHERE \`Username\`='${user}' `);
        if (accountInfos.length > 0) {
            let accountInfo = accountInfos[0];
            let qry = `SELECT * FROM \`cq_login\` WHERE \`account_id\`='${accountInfo.id}' and \`mac_adr\`='${mac}' and \`res_src\`='${res}' `;
            let loginToken = yield core_1.default.database.executeRaw(qry);
            if (loginToken.length > 0) {
                return true;
            }
        }
        return false;
    });
}
exports.validateAccess = validateAccess;
function loadInfo(user) {
    return __awaiter(this, void 0, void 0, function* () {
        let accountInfos = yield core_1.default.database.executeRaw(`SELECT * FROM \`accounts\` WHERE \`username\`='${user}' `);
        if (accountInfos.length > 0) {
            return accountInfos[0];
        }
        return false;
    });
}
exports.loadInfo = loadInfo;
//# sourceMappingURL=repository.js.map