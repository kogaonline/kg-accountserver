"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
var mysql = require('mysql');
let Connection = null;
class Database {
    create() {
        return __awaiter(this, void 0, void 0, function* () {
            let _conn = mysql.createPool({
                connectionLimit: 50,
                host: process.env.DB_HOST,
                user: process.env.DB_USER,
                password: process.env.DB_PASS,
                database: process.env.DB_BASE,
                charset: 'utf8mb4'
            });
            Connection = _conn;
            return yield new Promise(function (resolve, reject) {
                return __awaiter(this, void 0, void 0, function* () {
                    let connected = false;
                    while (!connected) {
                        connected = yield new Promise(function (resolve, reject) {
                            return __awaiter(this, void 0, void 0, function* () {
                                _conn.getConnection(function (err, connection) {
                                    if (err) {
                                        console.log(`Error connecting to DB: ${err.message}. Retrying in 10 seconds...`);
                                        setTimeout(() => resolve(false), 10000);
                                    }
                                    else {
                                        resolve(true);
                                        console.log('Connected to Database.');
                                    }
                                });
                            });
                        });
                    }
                    resolve(true);
                });
            });
        });
    }
    destroy() {
    }
    executeRaw(qry) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield new Promise(function (resolve, reject) {
                Connection.getConnection(function (err, connection) {
                    try {
                        if (connection) {
                            let result = connection.query(qry, function (err, rows, fields) {
                                if (err) {
                                    console.log(err);
                                    connection.release();
                                    resolve({ error: true });
                                }
                                else {
                                    connection.release();
                                    resolve(JSON.parse(JSON.stringify(rows)));
                                }
                            });
                        }
                        else {
                            console.log('NOCONNECTION');
                            resolve(this.executeRaw(qry));
                        }
                    }
                    catch (err) {
                        console.log('ERRDB');
                        console.log(err);
                    }
                });
            });
        });
    }
    getLastTableID(table) {
        return __awaiter(this, void 0, void 0, function* () {
            let qry = `SELECT \`id\` FROM \`${table}\` ORDER BY \`id\` DESC LIMIT 1`;
            return this.executeRaw(qry);
        });
    }
    filterQuery(string) {
        let newString = `${string}`;
        newString = newString.replace(new RegExp(`'`, 'g'), `\\'`);
        return newString;
    }
}
exports.default = Database;
//# sourceMappingURL=index.js.map