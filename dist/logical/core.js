"use strict";
exports.__esModule = true;
function getRandomInt() {
    var min = Math.ceil(0);
    var max = Math.floor(Number.MAX_SAFE_INTEGER);
    return Math.floor(Math.random() * (max - min)) + min;
}
exports.getRandomInt = getRandomInt;
function printBuffer(_buffer) {
    var print = '[';
    for (var x = 0; x < _buffer.length; x++) {
        var b = _buffer[x];
        print = print + b + ',';
    }
    print = print + ']';
    console.log("Printing packet: " + print);
}
exports.printBuffer = printBuffer;
