import * as colors from 'colors'

export default class ConsoleHandler {
    static setup(){
        ConsoleHandler.theme()
        const c = colors as any

        console.log=(function() {
            var orig=console.log;
            return function() {
                try {
                    let date = new Date()
                    let dateStr = c.default(`[${date.toLocaleDateString()} ${date.toLocaleTimeString()}]`)
                    arguments[0] = `${dateStr} ${arguments[0]}`
                    orig(...arguments)
                } finally {
                
              }
            };
        })();  

        console.error=(function() {
            var orig=console.error;
            return function() {
                try {
                    let date = new Date()
                    let dateStr = c.error(`[${date.toLocaleDateString()} ${date.toLocaleTimeString()}]`)
                    arguments[0] = `${dateStr} ${arguments[0]}`
                    orig(...arguments)
                } finally {
                
              }
            };
        })();  

        console.warn=(function() {
            var orig=console.warn;
            return function() {
                try {
                    let date = new Date()
                    let dateStr = c.warning(`[${date.toLocaleDateString()} ${date.toLocaleTimeString()}]`)
                    arguments[0] = `${dateStr} ${arguments[0]}`
                    orig(...arguments)
                } finally {
                
              }
            };
        })(); 
    }

    static theme(){
                
        colors.setTheme({
            default: 'cyan',
            error: 'red',
            warning: 'yellow',
        })

    }

}