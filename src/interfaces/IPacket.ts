export default interface IPacket
{
    ToArray() : Buffer
    Deserialize(buffer: Buffer): void;
    Send(client:any): void;
}