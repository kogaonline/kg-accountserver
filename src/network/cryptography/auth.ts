class CryptCounter {
    m_counter: number = 0

    createCounter(_with: number){
        this.m_counter = _with
    }

    get key2():any {
        return ((this.m_counter >> 8) & 0xFF);
    }

    get key1():any {
        return ((this.m_counter & 255) & 0xFF);
    }
    
    increment() { 
        this.m_counter = this.m_counter+1;
    }
}

export default class AuthCryptography {

    private _decryptCounter: CryptCounter
    private _encryptCounter: CryptCounter 
    private static _cryptKey1 : Buffer = null
    private static _cryptKey2 : Buffer = null
    private static _cryptKey3 : Buffer = null
    private static _cryptKey4 : Buffer = null
    private static Decrypt2 : boolean = false

    constructor(){
        this._encryptCounter = new CryptCounter();
        this._decryptCounter = new CryptCounter();
    }

    static prepareAuthCryptography(){
        if (AuthCryptography._cryptKey1 != null)
        {
            if (AuthCryptography._cryptKey1.length != 0)
                return;
        }
        AuthCryptography._cryptKey1 = Buffer.alloc(0x100);
        AuthCryptography._cryptKey2 = Buffer.alloc(0x100);
        let i_key1 = 0x9D;
        let i_key2 = 0x62;
        for (let i = 0; i < 0x100; i++)
        {
            AuthCryptography._cryptKey1[i] = i_key1;
            AuthCryptography._cryptKey2[i] = i_key2;
                        
            i_key1 = (((0x0F + ((i_key1 * 0xFA) & 0xFF)) * i_key1 + 0x13) & 0xFF);
            i_key2 = (((0x79 - ((i_key2 * 0x5C) & 0xFF)) * i_key2 + 0x6D) & 0xFF);
        }
    }

    encrypt(buffer:Buffer)
    {
        for (let i = 0; i < buffer.length; i++)
        {
            buffer[i] ^= 0xAB;
            buffer[i] = ((buffer[i] >> 4 | buffer[i] << 4) & 0xFF);
            buffer[i] ^= ((AuthCryptography._cryptKey1[this._encryptCounter.key1] ^ AuthCryptography._cryptKey2[this._encryptCounter.key2]) & 0xFF);
            this._encryptCounter.increment();
        }

        return buffer
    }

    decrypt(buffer:Buffer)
    {
        for (let i = 0; i < buffer.length; i++)
        {
            buffer[i] ^= 0xAB;
            buffer[i] = ((buffer[i] >> 4 | buffer[i] << 4) & 0xFF);
            buffer[i] ^= ((AuthCryptography._cryptKey2[this._decryptCounter.key2] ^ AuthCryptography._cryptKey1[this._decryptCounter.key1]) & 0xFF);
            this._decryptCounter.increment();
        }

        return buffer
    }
}