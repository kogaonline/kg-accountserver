import IPacket from '../../../interfaces/IPacket'
import Writer from '../BufferWriter'

export const FowardType = {
    Ready: 2,
    Invalid: 1,
    Banned: 12,
    NotActivated: 30,
    ActivateFailed: 31,
    TimedOut: 42,
    AttemptsUsed: 51,
}

export default class LoginFoward implements IPacket {
        _Buffer : Buffer
        
        constructor()
        {
            this._Buffer = Buffer.alloc(52)
            Writer.WriteUInt16(52, 0, this._Buffer);
            Writer.WriteUInt16(1055, 2, this._Buffer);
        }

        get Identifier():number {
            return this._Buffer.readUInt32LE(4);
        }
        set Identifier(value:number){
            Writer.WriteUInt32(value, 4, this._Buffer);
        }

        get Type():number {
            return this._Buffer.readUInt32LE(8);
        }
        set Type(value:number){
            Writer.WriteUInt32(value, 8, this._Buffer);
        }

        get IP():string {
            return this._Buffer.toString('utf8', 20, 20 + 16);
        }
        set IP(value:string){
            this._Buffer.write(value, 20)
        }

        get Port():number {
            return this._Buffer.readUInt16LE(12)
        }
        set Port(value:number){
            Writer.WriteUInt16(value, 12, this._Buffer);
        }

        Deserialize(buffer: Buffer){
            //no implementation
        }

        ToArray(): Buffer
        {
            return this._Buffer;
        }

        Send(client:any)
        {
            client.Send(this._Buffer);
        }
    }