import IPacket from '../../../interfaces/IPacket'
import Writer from '../BufferWriter'
import BufferConverter from '../BufferConverter'

export default class PasswordCryptographySeed implements IPacket {
        _Buffer : Buffer
        
        constructor()
        {
            this._Buffer = Buffer.alloc(8)
            Writer.WriteUInt16(8, 0, this._Buffer);
            Writer.WriteUInt16(1059, 2, this._Buffer);
        }

        get Seed():number {
            return this._Buffer.readInt32LE(4);
        }
        set Seed(value:number){
            Writer.WriteInt32(value, 4, this._Buffer);
        }

        Deserialize(buffer: Buffer){
            //no implementation
        }

        ToArray(): Buffer
        {
            return this._Buffer;
        }

        Send(client:any)
        {
            client.Send(this._Buffer);
        }
    }