import IPacket from '../../../interfaces/IPacket'
import Writer from '../BufferWriter'
import * as fs from 'fs'
import { printBuffer } from '../../../logical/kernel'

export default class Authentication implements IPacket {
    _buffer: Buffer

    constructor() {
        this._buffer = Buffer.alloc(312)
        Writer.WriteUInt16(312, 0, this._buffer);
        Writer.WriteUInt16(1542, 2, this._buffer);
    }

    username: string
    password: string
    server: string
    macAddr: string
    resource: number

    Deserialize(buffer: Buffer) {
        if (buffer.length === 312) {
            let data = buffer.toString('utf8', 0, 200)

            let user = data.substr(8, 16).replace(/\0/g, '')
            let pass = data.substr(120, 16).replace(/\0/g, '')
            let serv = data.substr(136, 16).replace(/\0/g, '')
            let mac = data.substr(152, 12).replace(/\0/g, '')
            let res = data.substr(193, 8).replace(/\0/g, '')

            this.username = user
            this.password = pass
            this.server = serv
            this.macAddr = mac
            this.resource = Number.parseInt(res, 10)

        }
    }

    ToArray(): Buffer {
        return this._buffer;
    }

    Send(client: any) {
        client.Send(this._buffer);
    }
}