export default class BufferConverter {
    static ToUInt32(buffer:Int8Array, offset:number){
        let _buffer = Buffer.from(buffer);
        var result = _buffer.readUInt32BE(offset);

        return result;
    }
    static ToInt32(buffer:Buffer, offset:number){
        var result = buffer.readInt32BE(offset);

        return result;
    }
    static ToUInt16(buffer:Int8Array, offset:number){
        let _buffer = Buffer.from(buffer);
        var result = _buffer.readUInt16BE(offset);

        return result;
    }
    static ToInt16(buffer:Int8Array, offset:number){
        let _buffer = Buffer.from(buffer);
        var result = _buffer.readInt16BE(offset);

        return result;
    }
    static ToUInt8(buffer:Int8Array, offset:number){
        let _buffer = Buffer.from(buffer);
        var result = _buffer.readUIntBE(offset, 1);
        return result;
    }
    static ToInt8(buffer:Int8Array, offset:number){
        let _buffer = Buffer.from(buffer);
        var result = _buffer.readIntBE(offset, 1);
        return result;
    }
    static ToByte(buffer:Int8Array, offset:number){
        let _buffer = Buffer.from(buffer);
        var result = _buffer.readIntBE(offset, 1);
        return result;
    }
}