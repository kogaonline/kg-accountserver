import * as net from 'net'
import AuthCrypto from '../cryptography/auth'
import IPacket from '../../interfaces/IPacket';
import Authentication from '../packets/auth/(1542) Authentication';

export default class AuthConnector {
    _socket: net.Socket
    cryptographer:AuthCrypto
    passwordSeed: number
    authInfo: Authentication

    constructor(socket :net.Socket){
        this._socket = socket
    }

    sendBuffer(buffer:Buffer){
        let _buffer = this.cryptographer.encrypt(buffer)
        this._socket.write(_buffer)
    }

    send(data:IPacket){
        this.sendBuffer(data.ToArray())
    }
}