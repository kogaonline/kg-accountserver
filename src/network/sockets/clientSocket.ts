import * as net from 'net'
import Core from '../../core';

export default class ClientSocket {
    client: net.Socket
    connected: boolean = false

    constructor(){
        this.client = new net.Socket();
    }

    connect(ip: any, port: any, resolve: any){
        let socket = this
        socket.client.on("end", (s: any) => {
            socket.connected = false
            console.log("Disconnected from Game Server...")
            Core.connectToGameServer()
        })

        return this.client.connect(port, ip, (s: any) => {
            socket.connected = true
            console.log('Connected to Game Server.')
            resolve(true)
        })
    }
 
    send(data: Array<any> | Buffer){
        this.client.write(data);
    }

    sendFowardInfo(id: number, username: string){
        let data = Buffer.alloc(8 + username.length);
        data.writeUInt32LE(id, 0)
        data.writeUInt32LE(username.length, 4)
        data.write(username, 8)

        this.send(data);
    }
}
