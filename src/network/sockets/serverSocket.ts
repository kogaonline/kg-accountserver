import * as net from 'net';
import Connector from '../../network/sockets/AuthConnector';
import AuthCryptography from '../../network/cryptography/auth';
import PasswordCryptographySeed from '../../network/packets/auth/(1059) PasswordSeed';
import LoginFoward, { FowardType } from '../../network/packets/auth/(1055) Foward';
import Authentication from '../../network/packets/auth/(1542) Authentication';
import { getRandomInt, printBuffer } from '../../logical/kernel';
import AccountInfo from '../../database/account/info';

export default class ServerSocket {
    server: net.Server
    constructor() {
        let serverSocket = this
        this.server = new net.Server(
            function (socket: net.Socket) {
                try {
                    socket.on('data', (data: Buffer) => serverSocket.receiveData(data, connector))

                    let pcs: PasswordCryptographySeed = new PasswordCryptographySeed()
                    pcs.Seed = getRandomInt()

                    let connector: Connector = new Connector(socket)
                    connector.cryptographer = new AuthCryptography()
                    connector.passwordSeed = pcs.Seed
                    connector.send(pcs)

                    socket.on('error', (data) => {
                        // console.log(`Socket error: ${data}`)
                    })
                } catch (err) {
                    console.log(err)
                }
            }
        );
    }

    start(port: number) {
        this.server.listen(port);
    }

    async receiveData(data: Buffer, connector: Connector) {
        try {
            let buffer = connector.cryptographer.decrypt(data);
            let length = buffer.readUInt16LE(0);
            let id = buffer.readUInt16LE(2);

            if (length != 312) {
                return;
            }

            let foward = new LoginFoward();
            foward.Type = FowardType.Invalid;

            connector.authInfo = new Authentication();
            connector.authInfo.Deserialize(buffer);

            const { username, password, server, resource, macAddr } = connector.authInfo;

            let accountInfo = new AccountInfo();
            await accountInfo.load(username);

            if (accountInfo.exists) {
                if (accountInfo.password === password) {
                    console.log(`Sending ${server} server info to ${username} -> ${process.env.GS_HOST}:${process.env.GS_PORT}.`);
                    foward.Identifier = accountInfo.id;
                    foward.Type = FowardType.Ready;
                    foward.IP = process.env.GS_HOST;
                    foward.Port = Number.parseInt(process.env.GS_PORT);
                }
                else console.log(`User ${username} sent wrong password.`);
            }
            else console.log(`User ${username} does not exist.`);

            connector.send(foward);
            console.log(`User ${username} access complete.`);
        } catch (err) {
            console.log(err.message);
            connector._socket.destroy();
        }
    }
}
