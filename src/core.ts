import Database from "./database";
import ConsoleHandler from "./console";
import ClientSocket from "./network/sockets/clientSocket";

export default class Core {
    static database : Database = new Database()
    static gameServerSocket : ClientSocket = null

    static async prepareDatabase(){
        console.log('Preparing Database...')
        return await Core.database.create()
    }

    static setupConsole(){
        ConsoleHandler.setup()
    }

    static async connectToGameServer(){
        Core.gameServerSocket = new ClientSocket()

        return await new Promise(async function (resolve: any, reject: any) {
            let connected:boolean = false
    
            while (!connected){
                connected = await new Promise<boolean>(async function (resolve: any, reject: any){
                    let socket = Core.gameServerSocket

                    await socket.connect('localhost', 9865, resolve).on('error', (err: Error) => {
                        console.log(err)
                        console.log('Retrying connection to Game Server...')
                        setTimeout(()=>{resolve(false)}, 5000)
                    })
                })
            }
    
            resolve(true)
        });
    }
}