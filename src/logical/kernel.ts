export function getRandomInt() {
    let min = Math.ceil(0);
    let max = Math.floor(Number.MAX_SAFE_INTEGER);
    return Math.floor(Math.random() * (max - min)) + min;
}

export function printBuffer(_buffer:Buffer){
    let print = '[';
    for (let x = 0; x < _buffer.length; x++){
        let b = _buffer[x];
        print = print + b + ',';
    }
    print = print + ']';

    let printh = '[';
    for (let x = 0; x < _buffer.length; x++){
        let b = _buffer[x].toString(16);
        printh = printh + '0x' + b + ',';
    }
    printh = printh + ']';
    console.log(`Printing packet: ${print}`)
    console.log(`Printing packet (hex): ${printh}`)
}