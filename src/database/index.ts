var mysql = require('mysql');

let Connection: any = null;

class Database {

  async create() {
    let _conn = mysql.createPool({
      connectionLimit: 50,
      host: process.env.DB_HOST,
      user: process.env.DB_USER,
      password: process.env.DB_PASS,
      database: process.env.DB_BASE,
      charset: 'utf8mb4'
    });

    Connection = _conn;

    return await new Promise(async function (resolve: any, reject: any) {
      let connected:boolean = false

      while (!connected){
        connected = await new Promise<boolean>(async function (resolve: any, reject: any){
           _conn.getConnection(function (err: any, connection: any) {
            if (err) {
                console.log(`Error connecting to DB: ${err.message}. Retrying in 10 seconds...`);
                setTimeout(()=> resolve(false), 10000)
            } else {
              resolve(true)
              console.log('Connected to Database.');
            }
          })
        })
      }

      resolve(true)
    });
  }

  destroy() {

  }

  async executeRaw(qry: string) {
    return await new Promise(function (resolve: any, reject: any) {
      Connection.getConnection(function (err: any, connection: any) {
        try {
          if (connection) {
            let result = connection.query(qry, function (err: any, rows: any, fields: any) {
              if (err) {
                console.log(err);
                connection.release();
                resolve({ error: true });
              } else {
                connection.release();
                resolve(JSON.parse(JSON.stringify(rows)));
              }
            });
          } else {
            console.log('NOCONNECTION');
            resolve(this.executeRaw(qry))
          }
        } catch (err) {
          console.log('ERRDB');
          console.log(err);
        }
      });

    });
  }

  async getLastTableID(table: string) {
    let qry = `SELECT \`id\` FROM \`${table}\` ORDER BY \`id\` DESC LIMIT 1`;
    return this.executeRaw(qry)
  }

  filterQuery(string: string) {
    let newString: string = `${string}`

    newString = newString.replace(new RegExp(`'`, 'g'), `\\'`)

    return newString
  }

}

export default Database;