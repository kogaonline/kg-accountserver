import Core from "../../core";

export async function validateAccess(user:string, mac:string, res:number = 0){
    let accountInfos:any = await Core.database.executeRaw(`SELECT * FROM \`accounts\` WHERE \`Username\`='${user}' `)
    if (accountInfos.length > 0){
        let accountInfo:any = accountInfos[0]

        let qry = `SELECT * FROM \`cq_login\` WHERE \`account_id\`='${accountInfo.id}' and \`mac_adr\`='${mac}' and \`res_src\`='${res}' `
        let loginToken:any = await Core.database.executeRaw(qry)
        if (loginToken.length > 0){
            return true
        }
    }

    return false
}

export async function loadInfo(user:string){
    let accountInfos:any = await Core.database.executeRaw(`SELECT * FROM \`accounts\` WHERE \`username\`='${user}' `)
    if (accountInfos.length > 0){
        return accountInfos[0]
    }
    return false
}