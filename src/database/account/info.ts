import { 
    loadInfo 
} from "./repository";

export default class AccountInfo {
    exists   : boolean = false;
    username : string;
    password : string;
    macAddr  : string;
    resource : number;
    id       : number;

    async load(user:string){
        let info:any = await loadInfo(user);
        if (info){
            this.exists = true;
            this.username = user;
            this.password = info.password;
            this.id = info.id;
        }
    }
}