import * as net from 'net'
import * as colors from 'colors'
import AuthCryptography from './network/cryptography/auth';
import Core from './core';
import ServerSocket from './network/sockets/serverSocket';
import ClientSocket from './network/sockets/clientSocket';

require('dotenv').config();

let startCore = new Promise(async (resolve: any, reject: any) => {

    console.log(colors.bgRed('\nStarting Account Server\n'))

    Core.setupConsole()
    AuthCryptography.prepareAuthCryptography()
    await Core.prepareDatabase()

    // All ok, start server
    resolve(true)

}).then(() => {
    //Core.connectToGameServer()

    let server = new ServerSocket()
    console.log(`Account Server started at port ${process.env.AS_PORT}.`)
    server.start(Number.parseInt(process.env.AS_PORT));

}).catch((err) => {
    console.log(err)
})