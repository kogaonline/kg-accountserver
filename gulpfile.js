var gulp = require("gulp");
watch = require('gulp-watch');

var typescript = require('gulp-tsc');

// all typescript files
var typeScriptFiles = [
    'src/**/*.ts'];

// set the tasks to run
gulp.task('default', ['compile-ts', 'tsc-watch']);


// watch all TypeScript files
gulp.task('tsc-watch', function() {
    watch(typeScriptFiles, function(file) {
        gulp.start('compile-ts');
    });
});

// compile TypeScript files into folder/ts.gen.js
gulp.task('compile-ts', function(){
  gulp.src(typeScriptFiles)
    .pipe(typescript({emitError: false}))
    .pipe(gulp.dest('dist/'));
});